<?php

use Restserver\Libraries\REST_Controller;

require(APPPATH . '/libraries/REST_Controller.php');

class Login extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Authorization_Token');
    }

    public function index_get()
    {
        $this->response([
            "test" => "Okeee"
        ], REST_Controller::HTTP_OK); 
    }
}